//config variables
var serverUrl = "http://avantajsoftwares.com/gruppo/json";
var clubImage = "http://www.clubbo.co.uk/";
var serverLargeImage = "http://www.avantajsoftwares.com/gruppo/uploads/medium/";
//var qurCodePath = "http://avantajsoftwares.com/gruppo/qrdirectory/";
//var facebookAccessToken ="274498962663336|bac3c9683cdd5a033858954270817b05";
var facebookAccessToken ="332690820161606|db9709da14b1f51a1124f0dbfc47ea63";
var QRCodeImagePath="http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=";

var lat;
var longe;
var keydata;
var Twitter;
var jsonObj;
var event_Tittle;
//var FB;

var monthArray = new Array();
	monthArray[01]= "Jan";
	monthArray[02]= "Feb";
	monthArray[03]= "Mar";
	monthArray[04]= "Apr";
	monthArray[05]= "May";
	monthArray[06]= "Jun";
	monthArray[07]= "Jul";
	monthArray[08]= "Aug";
	monthArray[09]= "Sep";
	monthArray[10]= "Oct";
	monthArray[11]= "Nov";
	monthArray[12]= "Dec";
    var onetimeClick=0;
    
function set_gps_location() {
	
				var onSuccess = function(position) {
					 lat = position.coords.latitude;
					 longe = position.coords.longitude;
					 //alert(lat);
				};
				// onError Callback receives a PositionError object
				function onError(error) {
					  alert('code: '    + error.code    + '\n' +
					  'message: ' + error.message + '\n');
				};
				navigator.geolocation.getCurrentPosition(onSuccess, onError);
				
}

function validator(){
		keydata = $('#keyword').val();
		if( initialCheckConnection()){
			if(keydata == ''){
				//alert("");
                navigator.notification.alert(
                                             "Empty value is not allowed for search",
                                             callBackFunctionB, // Specify a function to be called
                                             "Please enter keyword",
                                             "OK"
                                             );
                
			}else{
			  
			//if ($('#listing li').length == 0)
			//{
                $("#list_data").html('');
				var loadingImg ='<li><img class="loading" src="images/loading.gif" /></li>';
				$("#list_data").append(loadingImg);
				$.ajax({
					 //url:'http://www.clubbo.co.uk/mobile.php?lat=51.62038&long=0.30021',
					 //url:'http://www.clubbo.co.uk/mobile.php?lat=51.62038&long=0.30021&dest=Sugar%20Hut%20Village',
                   // url:'http://www.clubbo.co.uk/mobile.php?lat=51.62038&long=0.30021&dest=Su',

					 url:'http://www.clubbo.co.uk/mobile.php?lat='+lat+'&long='+longe+'&dest='+keydata,
					 type: 'GET',
					 dataType: "json",
					 success: function(data) {
						
						if(data.clubs)
							{
								var clubbo_data="";
								var i=0;
								var km;
                                var objs=data.clubs;
                                objs.sort(function(a,b){
                                          
                                          var a_distance = a.distance.replace(/[^0-9]+/g, '');
                                          var b_distance = b.distance.replace(/[^0-9]+/g, '');
                                          return (parseFloat(a_distance) > parseFloat(b_distance)) ? 1 : ((parseFloat(b_distance) > parseFloat(a_distance)) ? -1 : 0);                                 });
								$.each(objs, function(key, val) {
									if(val.distance !='0'){
										km = ' ';	
									}else{
										km = ' km';
									}
									if(val.id){
										clubbo_data += '<li id="'+val.id+'"><a data-transition="none" href="#details"><h3>'+val.title+'</h3><p>'+val.distance+''+ km+'</p></a></li>';
									}
									i++;
								});
								
								$("#list_data").html('');
								$("#headtag").html('');
								$("#counter").html('');
								
								$("#list_data").append(clubbo_data);
								$("#headtag").append(keydata);
								$("#counter").append(i+' Clubs');
								$("#list_data").listview("refresh");
								
								$('#list_data').delegate('li', 'click', function (event) {
									
									var id = $(this).attr("id");
								    club_details(id);
								
							   });
							}				
					 },
					 error: function (request, status, error) {
						//alert("Invalid keyword or Data not found");
                       navigator.notification.alert(
                                                    "Invalid keyword or Data not found",
                                                    callBackFunctionB, // Specify a function to be called
                                                    "Data not found",
                                                    "OK"
                                                    );
						window.location.href="#search";
					}
				});
				window.location.href="#listing";
		//}  //Loading image;
	}
  }		
}

function nearme(){
	
	if( initialCheckConnection()){
		
			
			//if ($('#listing li').length == 0)
			//{
                $("#list_data_nearme").html('');
				var loadingImg ='<li><img class="loading" src="images/loading.gif" /></li>';
				$("#list_data_nearme").append(loadingImg);
				if(lat == 0){
					//alert("Device GPS is offline");
                    navigator.notification.alert(
                                                 "Device GPS is offline",
                                                 callBackFunctionB, // Specify a function to be called
                                                 "Settings",
                                                 "OK"
                                                 );
                    
					window.location.href="#search";
				}
        //alert('http://www.clubbo.co.uk/mobile.php?lat='+lat+'&long='+longe);
					 $.ajax({
					 //url:'http://www.clubbo.co.uk/mobile.php?lat=51.62038&long=0.30021',
					 //url:'http://www.clubbo.co.uk/mobile.php?lat=51.62038&long=0.30021&dest=Sugar%20Hut%20Village',
					 url:'http://www.clubbo.co.uk/mobile.php?lat='+lat+'&long='+longe,
					 type: 'GET',
					 dataType: "json",
					 success: function(data) {
						if(data.clubs)
							{
								var clubbo_data="";
								var i=0;
                                var km;
                                var objs=data.clubs;
                                objs.sort(function(a,b){
                                          var a_distance = a.distance.replace(/[^0-9]+/g, '');
                                          var b_distance = b.distance.replace(/[^0-9]+/g, '');
                                          return (parseFloat(a_distance) > parseFloat(b_distance)) ? 1 : ((parseFloat(b_distance) > parseFloat(a_distance)) ? -1 : 0);
                                      });
								$.each(objs, function(key, val) {
									
									if(val.distance !='0'){
										km = ' ';	
									}else{
										km = ' km';
									}
									if(val.id){
										clubbo_data += '<li id="'+val.id+'"><a data-transition="none" href="#details"><h3>'+val.title+'</h3><p>'+val.distance+''+km+'</p></a></li>';
									}
									i++;
								});
								$("#list_data").empty();
								$("#list_data_nearme").html('');
								$("#headtag").html('');
								$("#nearme_counter").html('');
								$("#list_data_nearme").append(clubbo_data);
								$("#headtag").append(keydata);
								$("#nearme_counter").append(i+' Clubs');
								$("#list_data_nearme").listview("refresh");
								
								$('#list_data_nearme').delegate('li', 'click', function (event) {
									
									var id = $(this).attr("id");
								    club_details(id);
								
							   });
							}else{
                                 $("#list_data_nearme").html('');
								 $("#nearme_counter").html('0 Clubs');
							}				
					 },
					 error: function (request, status, error) {
						//alert("Data not found");
                        navigator.notification.alert(
                                                         "Data not found",
                                                         callBackFunctionB, // Specify a function to be called
                                                         "There is no data available for display",
                                                         "OK"
                                                         );
						window.location.href="#search";
					}
				});
			//}//Loading gif
	  }else{
		  window.location.href="#search";
	  }		 
}
	
function club_details(id){
    
	$(".date li.month-2").empty();
    $(".date li.month-1").empty();
    $(".date li.month-3").empty();
    
    $(".month li.month-1").empty();
    $(".month li.month-2").empty();
    $(".month li.month-3").empty();
    
    $("#Email").empty();
    $("#Map").empty();
    $("li.event_img").empty();
    $("#Web").empty();
    $("#logo").empty();
    $(".new-title").empty();
    $(".genre").empty();

    
    
    


    var calender_icon="";
    var current_month_array = new Array();
    var prev_month_array = new Array();
    var next_month_array = new Array();
    
    var DayImageArray = new Array("su","mo","tu","we","th","fr","sa");
    var monhNameArray = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
    
	if( initialCheckConnection()){
		$.ajax({
               //url:'http://www.clubbo.co.uk/mobile.php?club=1815',
               url:'http://www.clubbo.co.uk/mobile.php?club='+id,
               type: 'GET',
               dataType: "json",
               success: function(data) {
               
//               if(data.club.events.id==null)
//               {
//              // alert("No events found");
//               }
//               else
//               {
			  // window.location.href="#details";
               
               var club_logos ='';
               var call_now;
               if(data.club.events.id !=null)
               {
                    var event_date = data.club.events.date;
                    var event_Tittle = data.club.events.title;
                    var split_result = event_date.split(',');
               }
               //alert(split_result[0]);
               jsonObj = [];
               
               var d=new Date();
               //d.formate('YYYY/mm/dd');
               var curr_date = d.getDate();
               //var prev_month = d.getMonth();
               var curr_month = d.getMonth() + 1; //Months are zero based
               //var next_month = d.getMonth() + 2;
               var curr_year = d.getFullYear();
               var dated = curr_month + "/" + curr_date + "/" + curr_year;
               var nextMonthDate = new Date(d.getFullYear(), d.getMonth() + 1);
               var prevMonthDate = new Date(d.getFullYear(), d.getMonth() - 1);
               
               // alert(curr_month);
               
               var arr = new Array();
               var prev_arr = new Array();
               var next_arr =new Array();
               for(i = 0; i < 31; i++){
                    arr[i]=false;
                    prev_arr[i]=false;
                    next_arr[i]=false;
               
               }
               var countVar =0;
               var prev_countVar =0;
               var next_countVar =0;
               
               if(data.club.events.id !=null)
               {
                for(j = 0; j < split_result.length; j++){
               
                    if(dated <= split_result[j]){
                        var dated = split_result[j];
                    }else{
                        var dated = split_result[j];
                    }
                    jsonObj.push({title: data.club.events.title, dtstart: split_result[j]});
                    var cal_d = new Date(jsonObj[j].dtstart);
                    var cal_curr_month = cal_d.getMonth();
                    var cal_curr_date = cal_d.getDate();
               
                    if(curr_month == cal_curr_month+1){
                        arr[cal_curr_date] =true ;
                        current_month_array[countVar] =split_result[j];
                        countVar++;
                    }
                    //alert(nextMonthDate.getMonth());
                    if(prevMonthDate.getMonth() == cal_curr_month){
                        prev_arr[cal_curr_date] =true ;
                        prev_month_array[prev_countVar] =split_result[j];
                        prev_countVar++;
                    }
                    if(nextMonthDate.getMonth() == cal_curr_month){
                        //alert("next");
                        next_arr[cal_curr_date] =true ;
                        next_month_array[next_countVar] =split_result[j];
                        next_countVar++;
                    }
               
                }
               }

               jsonObj = [];
               if(data.club.events.id !=null)
               {

                    jsonObj.push({title: data.club.events.title, dtstart: split_result[0]});
               }

               var countVar =0;
               var precountVar =0;
               var nextcountVar =0;
               //var currentMonthSplit = current_month_array[0].split('/');
			   
               
               var year = curr_year;
               var month = curr_month;
               var days = Math.round(((new Date(year, month))-(new Date(year, month-1)))/86400000);
               
               //current month
               //var dayyy = dateDay.getMonth();
              // alert(dayyy);
               for(i = 1; i <= days; i++)
               {
                    var dateDay = new Date( curr_year, curr_month-1, i ) ;
                    if(arr[i]==true){
                        var dayyy = dateDay.getMonth();
                        // alert(current_month_array[2]);
                        calender_icon += '<a class="'+DayImageArray[dateDay.getDay()]+' purple" href="#" id="'+current_month_array[countVar]+'">'+i+'</a>';
                        countVar++;
                    }
                    else{
                        calender_icon += '<a class="'+DayImageArray[dateDay.getDay()]+'" href="#" >'+i+'</a>';
                    }
               }
               $(".date li.month-2").empty();
               $(".date li.month-2").append(calender_icon);
               
               
               //prevmonth dates
               var precalender_icon="";
               // alert(prevMonthDate.getYear());
               var predays = Math.round(((new Date(prevMonthDate.getFullYear(), prevMonthDate.getMonth()+1))-(new Date(prevMonthDate.getFullYear(), prevMonthDate.getMonth())))/86400000);
               for(i = 1; i <= predays; i++)
               {
                    var dateDay = new Date(prevMonthDate.getFullYear(), prevMonthDate.getMonth(), i ) ;
                    if(prev_arr[i]==true){
                        // alert(current_month_array[2]);
                        precalender_icon += '<a class="'+DayImageArray[dateDay.getDay()]+' purple" href="#" id="'+prev_month_array[precountVar]+'">'+i+'</a>';
                        precountVar++;
                    }
                    else{
                        precalender_icon += '<a class="'+DayImageArray[dateDay.getDay()]+'" href="#" >'+i+'</a>';
                    }
               }
               
               $(".date li.month-1").empty();
               $(".date li.month-1").append(precalender_icon);
               
               //nextmonth dates
               var nextcalender_icon="";
               
               var nextdays = Math.round(((new Date(nextMonthDate.getFullYear(), nextMonthDate.getMonth()+1))-(new Date(nextMonthDate.getFullYear(), nextMonthDate.getMonth())))/86400000);
               for(i = 1; i <= nextdays; i++)
               {
                    var dateDay = new Date( nextMonthDate.getFullYear(), nextMonthDate.getMonth(), i ) ;
                    if(next_arr[i]==true){
                        // alert(current_month_array[2]);
                        nextcalender_icon += '<a class="'+DayImageArray[dateDay.getDay()]+' purple" href="#" id="'+next_month_array[nextcountVar]+'">'+i+'</a>';
                        nextcountVar++;
                    }
                    else{
                        nextcalender_icon += '<a class="'+DayImageArray[dateDay.getDay()]+'" href="#" >'+i+'</a>';
                    }
               }
               
               $(".date li.month-3").empty();
               $(".date li.month-3").append(nextcalender_icon);
               
               $(".month li.month-1").empty();
               $(".month li.month-2").empty();
               $(".month li.month-3").empty();
               
               $(".month li.month-1").append(monhNameArray[prevMonthDate.getMonth()]);
               $(".month li.month-2").append(monhNameArray[curr_month-1]);
               $(".month li.month-3").append(monhNameArray[nextMonthDate.getMonth()]);
               //month
               //month-1
               
               $(".purple").bind('touchstart', function(event){
                                 jsonObj = [];
                                 jsonObj.push({title: event_Tittle, dtstart: event.target.id});
                                 $('.date > li > a.blue').addClass('purple').removeClass('blue');
                                 $(this).addClass('blue');
                                 });
               
			   $(".pp").empty();
               var pp_link = '<a href="#" onclick="calender_sync()"></a>';
               $(".pp").append(pp_link);
               
               if(data.club.logo){
                    club_logos += '<img style="width:75px; height:75px;" src="'+clubImage+data.club.logo+'" >';
               }else{
                    club_logos += '<img src="images/clubLogo.jpg" >';
               }
               call_now ='<a href="tel:'+data.club.phone+'">Call</a>';
               
               if(data.club.email){
                    email_now ='<a id="'+data.club.email+'" onclick="showMailComposer()">Email</a>';
                }else{
                    email_now ='<a href="#">Email</a>';
			   }
			   
			   if(data.club.web){
                    web_now ='<a class="webDetails" id="'+data.club.web+'" href="#">Web</a>';
                    //web_now ='<li><a href="" onClick="webView('data.club.web');">Web</a></li>';
			   }else{
                    web_now ='<a href="#">Web</a>';
			   }
			   
			   if(data.club.address){
                    var map_url = 'https://maps.google.co.uk/maps?q='+data.club.title+', '+data.club.address+'&hl=en&t=m&z=14&iwloc=A';
               
                    var address_map = '<a class="mapDetails" id="'+map_url+'"  href="#">Map</a>';
			   }else{
                    var address_map = '';
			   }
			   if(data.club.events.logo=="images/events/" || !data.club.events.logo)
               {
                    club_img = '<img class="img" src="../no-image.png" />';
               }
               else{
                    club_img = '<img class="img" src="'+clubImage+data.club.events.logo+'" />';
                    //alert(clubImage+data.club.events.logo);
			   }
               if(data.club.events.description){
               club_img += '<p>'+data.club.events.description+'</p>';
               //alert(clubImage+data.club.events.logo);
			   }

               
			   
			   if(data.club.title){
                    club_title ='<h>'+data.club.title+'</h><p style="font-size:10px; color:3cfcfcf;">'+data.club.address+'</p>';
               
               social_event_name = '<input type="hidden" name="clubb_title" value="'+data.club.title+'" id="clubb_title"/>';
               social_venu_name = '<input type="hidden" name="clubb_venu" value="'+data.club.address+'" id="clubb_venu"/>';
               social_venu = '<input type="hidden" name="clubb_venu_name" value="'+data.club.events.title+'" id="clubb_venu_name"/>';
			   }
			   
			   if(data.club.genre){
                    club_genre =data.club.genre;
			   }
			   
               
               $("#Email").empty();
			   $("#Map").empty();
               $("li.event_img").empty();
               $("#Web").empty();
               $("#logo").empty();
			   $(".new-title").empty();
			   $(".genre").empty();
			   
               $("#callnow").html('');
               $("#logo").append(club_logos);
               $("#callnow").append(call_now);
               $("#Email").append(email_now);
               $("#Web").append(web_now);
			   $(".new-title").append(club_title);
               $(".new-title").append(social_event_name);
               $(".new-title").append(social_venu_name);
               $(".new-title").append(social_venu);
			   $(".genre").append(club_genre);
			   $("#Map").append(address_map);
               $("li.event_img").append(club_img);
              // }
               
               $(".webDetails").bind('touchstart', function(event){
                                     //alert(event.target.id);
                                     window.plugins.childBrowser.showWebPage(event.target.id, { showLocationBar: false });
                                     
                                     });
			   
			   $(".mapDetails").bind('touchstart', function(event){                                     var newString =event.target.id.replace(/ /g,"+");
                                        window.plugins.childBrowser.showWebPage(newString, { showLocationBar: false });
                                     
                                     });
			   
               },
               error: function (request, status, error) {
               //alert("Data not found");
               navigator.notification.alert(
                                            "Data not found",
                                            callBackFunctionB, // Specify a function to be called
                                            "There is no data available for display",
                                            "OK"
                                            );
            }
        }); 
	}
}



function showMailComposer()
{
    if( initialCheckConnection()){ 
        window.plugins.emailComposer.showEmailComposerWithCallback(function(result){console.log(result);},"Event Details","Take a look at <b>this<b/>:",[],[],[],true,[]);
    }
}


function calender_sync(){
	
   // alert("hai");
	if(jsonObj=="")
    {
        
        navigator.notification.alert(
                                     "There is no event for sync!",
                                     callBackFunctionB, // Specify a function to be called
                                     "No Event",
                                     "OK"
                                     );
      
        
        
    }
    else
    {
        var SyncPlugin = {
        
        callNativeFunction: function (success, fail, resultType) {
            return Cordova.exec(success, fail, "ATcalenderSync", "nativeFunction", [resultType]);
            }
        };

        SyncPlugin.callNativeFunction( nativePluginResultHandler, nativePluginErrorHandler,jsonObj);

    }
    //alert("poii");
}


function callBackFunctionB(){
    //console.log('ok');
}


 function ViewMap(){
	 
	 if( initialCheckConnection()){ 
	   window.plugins.childBrowser.showWebPage("https://maps.google.co.uk/maps?q=Sugar+Hut+Village,+High+Street,+Brentwood&hl=en&ll=51.619988,0.305128&spn=0.034532,0.043259&sll=52.8382,-2.327815&sspn=8.605948,11.074219&oq=sugar+hut+&hq=Sugar+Hut+Village,+High+Street,+Brentwood&t=m&z=14&iwloc=A", { showLocationBar: false });

	 }
 }

 function twitter_share(){
     if( initialCheckConnection()){ 
              window.plugins.childBrowser.showWebPage('https://twitter.com/intent/tweet?status=I+am+using+Clubbo+to+Party!+%20%23Clubbo&amp;', { showLocationBar: false });
     }

		 //u=encodeURIComponent("http://www.avantajsoftwares.com");
	     //tweet=encodeURIComponent("Hello");
	     //window.plugins.childBrowser.showWebPage('https://twitter.com/intent/tweet?text='+tweet+', { showLocationBar: false });
 }

function twitter_share_club(){
    if( initialCheckConnection()){ 
        var club_title = $('#clubb_title').val().replace(/ /g,"+");
        var club_venu_name = $('#clubb_venu_name').val().replace(/ /g,"+");
        var club_venu = $('#clubb_venu').val().replace(/ /g,"+");
    
        if(club_venu_name == "null"){
            navigator.notification.alert(
                                     "There is no event to tweet",
                                     callBackFunctionB, // Specify a function to be called
                                     "No Event",
                                     "OK"
                                     );
        }else{
            if( initialCheckConnection()){ 
                window.plugins.childBrowser.showWebPage('https://twitter.com/intent/tweet?status=This+is+where+I+am+partying+'+club_title+','+club_venu_name+','+club_venu+'', { showLocationBar: false });
            }
        }
    }
}
		
 function facebook_share() {
     
     var club_title = 'Clubbo.co.uk';
     var club_venu_name = '+';
     var club_venu = '+';
     if( initialCheckConnection()){ 

         window.plugins.childBrowser.showWebPage('https://www.facebook.com/dialog/feed?app_id=120226568157370&link=http://www.clubbo.co.uk/&name=I+am+using+Clubbo+to+Party!+'+club_title+'&caption='+club_venu_name+'&description='+club_venu+'&redirect_uri=https://www.facebook.com', { showLocationBar: false });
     }
	    
 }
function facebook_share_club() {
    
    
    
    var club_title = $('#clubb_title').val().replace(/ /g,"+");
    var club_venu_name = $('#clubb_venu_name').val().replace(/ /g,"+");
    var club_venu = $('#clubb_venu').val().replace(/ /g,"+");

    
    if(club_venu_name == "null"){
        navigator.notification.alert(
                                     "There is no event to share",
                                     callBackFunctionB, // Specify a function to be called
                                     "No Event",
                                     "OK"
                                     );
         }else{
             if( initialCheckConnection()){ 
    
                 window.plugins.childBrowser.showWebPage('https://www.facebook.com/dialog/feed?app_id=120226568157370&link=http://www.clubbo.co.uk/&name=This+is+where+I+am+partying!+'+club_title+'&caption='+club_venu_name+'&description='+club_venu+'&redirect_uri=http://www.facebook.com', { showLocationBar: false });
             }
        }
}

 function special_map(){
	 
	 var stockholm = new google.maps.LatLng(59.32522, 18.07002);
     var parliament = new google.maps.LatLng(59.327383, 18.06747);
     var marker;
     var map;

     function initialize() {
    	 //alert("Hai");
       var mapOptions = {
         zoom: 13,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
         center: stockholm
       };

       map = new google.maps.Map(document.getElementById('map_canvas'),
               mapOptions);

       marker = new google.maps.Marker({
         map:map,
         draggable:true,
         animation: google.maps.Animation.DROP,
         position: parliament
       });
       google.maps.event.addListener(marker, 'click', toggleBounce);
     }

     function toggleBounce() {

       if (marker.getAnimation() != null) {
         marker.setAnimation(null);
       } else {
         marker.setAnimation(google.maps.Animation.BOUNCE);
       }
     }
     window.location.href="#list_data_map";
 }
	  
/****************************************************************************************************************************/ 
 	
 	 		
 		
/****************************************************************************************************************************/ 		
 		
 
//showSocialButtons();


  function initialCheckConnection() {
   var networkState = navigator.network.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';
    if(states[networkState]==states[Connection.NONE] || states[networkState]==states[Connection.UNKNOWN]){
       // alert('This app needs a network connection');
        navigator.notification.alert(
                                     "This app needs a network connection",
                                     callBackFunctionB, // Specify a function to be called
                                     "Connectivity",
                                     "OK"
                                     );
        return false;

        //device.exitApp();
        //window.location.href="#noInternet";
    } 	
    else{
        return true;
    	//initialCheck();
		}
}
  

















