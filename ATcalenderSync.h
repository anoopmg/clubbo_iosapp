//
//  ATcalenderSync.h
//  Clubbo
//
//  Created by APPLE on 15/02/13.
//  Copyright (c) 2013 Avantaj. All rights reserved.
//

#import <Cordova/CDV.h>
#import <EventKit/EventKit.h>

@interface ATcalenderSync : CDVPlugin

@property int calenderPermissionIos6;
- (void) nativeFunction:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options;

@end
